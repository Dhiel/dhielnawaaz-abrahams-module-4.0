import 'package:flutter/material.dart';
import 'package:saphca/dashboard.dart';

void main() => runApp(const Appointment());

class Appointment extends StatelessWidget {
  const Appointment({Key? key}) : super(key: key);

  static const String _title = 'Appointment booking';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.message),
              label: ' My Chat Community',
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.access_time),
              label: 'My Scedualed Appointments',
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_box),
              label: 'My Profile',
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: 'Settings',
              backgroundColor: Colors.blue,
            ),
          ],
        ),
        body: const MyStatefulWidget(),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

enum OS { Doctor, Dentist, Dietitian }

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  OS? _os = OS.Doctor;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          const SizedBox(
            height: 30,
          ),
          const Text('I would like to make an appointment booking for?',
              style: TextStyle(fontSize: 25)),
          const SizedBox(
            height: 30,
          ),
          ListTile(
              title: const Text('Doctor'),
              leading: Radio<OS>(
                value: OS.Doctor,
                groupValue: _os,
                onChanged: (OS? value) {
                  setState(() {
                    _os = value;
                  });
                },
              )),
          ListTile(
              title: const Text('Dentist'),
              leading: Radio<OS>(
                value: OS.Dentist,
                groupValue: _os,
                onChanged: (OS? value) {
                  setState(() {
                    _os = value;
                  });
                },
              )),
          ListTile(
              title: const Text('Dietitian'),
              leading: Radio<OS>(
                value: OS.Dietitian,
                groupValue: _os,
                onChanged: (OS? value) {
                  setState(() {
                    _os = value;
                  });
                },
              )),
          Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                child: const Text('Process request'),
                onPressed: () {},
              )),
          TextButton(
            onPressed: () {
              //cancel appoitment request
            },
            child: const Text(
              'Changed your mind?',
            ),
          ),
          Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                child: const Text('Cancel request'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Dashboard()),
                  );
                },
              )),
        ],
      ),
    );
  }
}
